---
title: Homepage
bookToc: false
type: main
---

# Autophotographer

This project aims to automate the process of selecting "aesthetic" images from a video feed.
