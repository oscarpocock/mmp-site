---
title: "Week 8"
date: 2022-03-27T13:13:48+01:00
draft: false
---
# Monday
Towards the end of last week ([Week 7](../week-7)), I managed to refactor my code in order to make it more portable. This allowed me to train my model on different machines. I ran my training script on the uni's GPU compute successfully for 20 epochs. The next stage was to train it for longer and analyse the results. On the Monday morning I adjusted the parameters of my training script to train for 2000 epochs instead.

# Tuesday
Tuesday afternoon the training had finished and I had a model that was trained on 2000 epochs. This gave me a day to analyse the results and do some rough predictions before my mid-project demo on the Wednesday.
## Training and validation loss graphs
![training and validation loss 20 epochs](/20-epoch-plot.png)
![training and validation loss 2000 epochs](/2000-epoch-plot.png)

As we can see from the 2000 Epochs graph, the loss seems to plateau at around 60 epochs. The training loss seems to even out with more accuracy than the validation loss. This means that our data isn't fully learning what I want it to. Also it's overfitting slightly as it's better at predicting the training set than the validation set. The variance in the validation set shows that the features it's decided to learn aren't the right features to confidently predict aesthetics in this dataset.

For the rest of the day I worked on my prediction script so I could use the model to predict new pictures. I also worked on my architecture diagrams and slides for the mid-project demo.

![images with prediction scores](/predictions.png)

Due to the nature of how I processed my images (resizing them to 32x32 and then saving them to a tensor then saving them to disk), my prediction script also displayed those down-sized images. This may have also effected the performance of the model.

# Wednesday
I spent most of Wednesday morning finishing my slides, diagrams and making example predictions using the prediction script.

![overall project architecture](/project-model.svg)

# Rest of week
I spent the rest of the week looking at the project's overall pipeline including the non-machine learning filtering. I also started to implement basic focus detection by looking at blur detection using the Laplacian operator[^1].
[^1]:https://pyimagesearch.com/2015/09/07/blur-detection-with-opencv/
