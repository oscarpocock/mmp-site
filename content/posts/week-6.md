---
title: "Week 6"
date: 2022-03-13T12:40:17+01:00
draft: false
---

This week I finished programming the basic CNN model using transfer learning. I decided to train it for 20 epochs to make sure there weren't any runtime errors in my code. As I don't own an Nvidia GPU (I have an AMD GPU), I couldn't make use of the pytorch version that utilised CUDA to speed up processing. There is a RocM version of pytorch for AMD GPUs[^1] but RocM isn't as mature as CUDA and only officially supports a small subset of Linux distributions. Therefore, for this model I trained on the COU for only 20 epochs just to make sure it ran successfully before trying to find some Nvidia compute.
[^1]: https://pytorch.org/blog/pytorch-for-amd-rocm-platform-now-available-as-python-package/

## Transfer learning model architecture
### Full
![Transfer learning architecture](/transfer-cnn-arch.svg)
### Simplified
![Simplified Transfer learning architecture](/transfer-cnn-arch-simpl.svg)
## Validation and train loss over 20 epochs
![plot of training and validation loss over 20 epochs](/20-epoch-plot.png)
