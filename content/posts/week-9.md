---
title: "Week 9"
date: 2022-04-03T13:13:48+01:00
draft: false
---
This week I worked on getting focus detection working. I implemented basic laplatian blur detection[^1] and fast fourier blur detection[^2].
[^1]:https://pyimagesearch.com/2015/09/07/blur-detection-with-opencv/
[^2]:https://pyimagesearch.com/2020/06/15/opencv-fast-fourier-transform-fft-for-blur-detection-in-images-and-video-streams/

Finding the threshold for both can be a challenge

# General pipeline
I continued to develop a general pipeline to fit all the filters in
