---
title: "Week 4"
date: 2022-02-27T12:46:59Z
draft: false
---
This week I did some research into how to build a CNN from scratch, including the different type of layers, loss functions, learning rates, epochs and other core concepts.[^1][^2]
[^1]: deeplizard. "Convolutional Neural Networks (CNNs) explained." (Dec. 9, 2017). Accessed: Feb. 22, 2022. [Online Video]. Available: https://youtube.com/watch?v=YRhxdVk_sIs
[^2]: A Rosebrock. "PyTorch: Training your first Convolutional Neural Network (CNN)". pyimagesearch.com. https://pyimagesearch.com/2021/07/19/pytorch-training-your-first-convolutional-neural-network-cnn/ (accessed Feb. 22, 2022.)

I also set up and created this blog with [Hugo](https://gohugo.io/) to document my progress and setup [Woodpecker CI](https://woodpecker-ci.org/) to do continuous testing and integration.
