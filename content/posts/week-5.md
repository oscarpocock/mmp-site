---
title: "Week 5"
date: 2022-03-06T12:40:14+01:00
draft: false
---
# Starting to write a CNN
This week I started to implement what I had learnt about CNNs in [Week 4](../week-4/). At this point I hadn't designed a CNN architecture to implement, instead I wanted to have a running model regardless of performance just to see if I could implement one and understand it.

Half way through implementation, I decided to look back at the existing research papers on judging aesthetic judgement to see which aspects of their system and CNN were important to the task. While reading I saw that nearly all research papers on the topic used transfer learning instead of creating their own CNNs from scratch. At this point I stopped writing a CNN from scratch and decided to solve my problem using transfer learning instead. I followed a guide[^1] on implementing transfer learning in pytorch and another guide[^2][^3] on creating a regressive CNN model.
[^1]:A Rosebrock. "PyTorch: Transfer Learning and Image Classification". pyimagesearch.com. https://pyimagesearch.com/2021/10/11/pytorch-transfer-learning-and-image-classification/ (accessed Mar. 1, 2022.)
[^2]:A Rosebrock. "Regression with Keras". pyimagesearch.com. https://pyimagesearch.com/2019/01/21/regression-with-keras/ (accessed Feb. 22, 2022.)
[^3]:A Rosebrock. "Keras, Regression, and CNNs". pyimagesearch.com. https://pyimagesearch.com/2019/01/28/keras-regression-and-cnns/ (accessed Feb. 22, 2022.)
