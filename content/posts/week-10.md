---
title: "Week 10"
date: 2022-04-10T13:13:48+01:00
draft: false
---
This week I was busy applying for jobs and working on the CS38220 assignment. So I only managed to do code clean-up and implementing a config file which allows a user to change the order of filters and other settings.

Config file example: `config.yml`

```yaml {linenos=table, linenostart=1}
---
# Config file for autophotographer

# List of filters to apply in order
filters:
  - brightness
  - filesize
  - contrast
  - focus

# Whether or not to apply CNN ranking
CNNrank: True

# Options for focus filter
brightness_options:
  threshold: 0.35

# Options for focus filter
filesize_options:
  threshold: 0.35

# Options for focus filter
contrast_options:
  threshold: 0.35

# Options for focus filter
focus_options:
  threshold: 0.35
...
```
