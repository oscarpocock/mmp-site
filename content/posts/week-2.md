---
title: "Week 2"
date: 2022-02-13T12:46:54Z
---
This week I set up my repositories and starting writing some basic code.

# Set up
Before starting any coding, I wanted to set up my remote git repositories. I had already decided I wanted the project mirrored over two remote git repositories from different providers as a safety precaution. My intial plan was to use the university's GitLab instance but as it's recently been moved behind the firewall it would have made mirroring quite difficult. Instead, I decided to use my personal [Gitea instance](https://git.oscar.blue) and mirror it to my personal account on the official [GitLab instance](https://gitlab.com/oscarpocock).

[Gitea](https://git.oscar.blue/noble/mmp-osp1) → [GitLab](https://gitlab.com/oscarpocock/mmp-osp1)

Gitea will periodically push to GitLab.

# Code
Towards the end of the week I put together some simple code to reduce the frame set. I used the OpenCV Python module to export the frames of a given video to an `output/` folder. The path of this folder is also stored in the `outputfolder` variable we see used on line 7.

Although this code is a first step at reducing the frames set - it doesn't give us much feedback about the frames that have been deleted or kept.

```python {linenos=table, linenostart=1}
def display_file_sizes():
"""Calculates the average frame filesize and deletes the frames lower
than the average
"""
    # create an array storing the filesize of each frame
    filesizes = []
    for filename in os.listdir(outputfolder):
        filepath = outputfolder + "/" + filename
        filesize = os.path.getsize(filepath)
        print(filepath + ": " + str(filesize))
        filesizes.append(filesize)

    # work out average
    average = sum(filesizes)/len(filesizes)
    print ("Average is: " + str(average))

    # delete files below average
    count = 0
    for filename in os.listdir(outputfolder):
        filepath = outputfolder + "/" + filename
        if filesizes[count] < average:
            # print(filepath + ": " + str(filesizes[count]))
            os.remove(filepath)
        count += 1
```
I didn't end up doing as much as I would have liked this week. I found myself preoccupied in other areas of my life and not putting enough emphasis on this project.

# 1:1 Weekly meeting
- As I didn't do as much as I would like to have done there wasn't much to discuss on my part
- I needed to produce more code to have some basic functionality.
- Discussed my concerns with a machine learning approach 
