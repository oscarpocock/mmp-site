---
title: "Week 3"
date: 2022-02-20T12:46:55Z
---

# Filming footage 
At the start of the week I went into town to film some practise footage to work with later (up until this point I had been experimenting with footage limited by my bedroom walls). I took some basic vertical and horizontal footage of the town - no nature or breach footage yet.

# Gaining more useful information
I used the footage I had recorded at the start of the week and revised my "filesize" code. I made a new function `order_frames_by_filesize()` which orders the frames by the filesize and prints the name of size of each frame in order.
```python {linenos=table}
def order_frames_by_filesize():
    """Order the frames by filesize and print the filenames and their sizes"""
    frames = os.listdir(outputfolder)

    # sort the frames by their filesize
    frames = sorted(frames, 
    key = lambda x: os.stat(os.path.join(outputfolder, x)).st_size, reverse = True)
    
    # print every frame and it's size in a human readable format
    for frame in frames:
        filesize = os.stat(os.path.join(outputfolder, frame)).st_size
        if filesize > 1024:
            filesize = filesize / 1024
            print(frame + ": " + str(filesize) + " KB")
        else:
            print(frame + ": " + str(filesize))
```

# Analysing datasets
There's been a few datasets that I've previously looked at which could be useful to use for this project. I wasn't sure how they would perform with the data I was expecting to use. AVA[^1] was trained on data that had been photographed under ideal conditions by profesionals. This won't reflect the frames extracted by the footage in this project's use case. I wasn't sure if this distinction was significant enough to effect the results of a trained model. I had previously found a project[^2] which had models pretrained using AVA[^1] I could use to predict an aesthetic value from images I provided.
[^1]: N. Murray, L. Marchesotti and F. Perronnin, "AVA: A large-scale database for aesthetic visual analysis," 2012 IEEE Conference on Computer Vision and Pattern Recognition, 2012, pp. 2408-2415, doi: 10.1109/CVPR.2012.6247954. 
[^2]:Image Quality Assessment: https://github.com/idealo/image-quality-assessment - Idealo

# 1:1 Weekly meeting
- We discussed that during week 4 I should be looking at implementing CNNs as training might take a while and therefore should be a priority.
- Look at what other people are doing with aesthetic analysis to get an idea on how the code works.
- Attempt to get a basic CNN working.

